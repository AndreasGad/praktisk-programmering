#include <stdio.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_eigen.h>
#include <gsl/gsl_linalg.h>

#define TRACE(args...) //fprintf(stderr,args)
const double pi = 3.14159265358979323844;


int main(){

    // Solve the equation A*x = b
    gsl_matrix *A = gsl_matrix_alloc(3,3);
    gsl_matrix_set(A,0,0, 6.13);
    gsl_matrix_set(A,0,1,-2.90);
    gsl_matrix_set(A,0,2, 5.86);
    gsl_matrix_set(A,1,0, 8.08);
    gsl_matrix_set(A,1,1,-6.31);
    gsl_matrix_set(A,1,2,-3.89);
    gsl_matrix_set(A,2,0,-4.36);
    gsl_matrix_set(A,2,1, 1.00);
    gsl_matrix_set(A,2,2, 0.19);

    gsl_vector *b = gsl_vector_alloc(3);
    gsl_vector_set(b,0,6.23);
    gsl_vector_set(b,1,5.37);
    gsl_vector_set(b,2,2.29);

    gsl_vector *x = gsl_vector_alloc(3);

    gsl_linalg_HH_solve(A,b,x);

    printf("\nThe solution to A*x=b is:\n");
    printf("|%3.2lg\t %3.2lg\t%3.1lg|    |%3.2lg|   |%3.2lg|\n",gsl_matrix_get(A,0,0),gsl_matrix_get(A,0,1),gsl_matrix_get(A,0,2),gsl_vector_get(x,0),gsl_vector_get(b,0));
    printf("|%3.2lg\t%3.2lg\t %3.2lg|  * |%3.2lg| = |%3.2lg|\n",gsl_matrix_get(A,1,0),gsl_matrix_get(A,1,1),gsl_matrix_get(A,1,2),gsl_vector_get(x,1),gsl_vector_get(b,1));
    printf("|%3.2lg\t%3.2lg\t %3.2lg|    | %3.1lg|   |%3.2lg|\n\n",gsl_matrix_get(A,1,0),gsl_matrix_get(A,2,1),gsl_matrix_get(A,2,2),gsl_vector_get(x,2),gsl_vector_get(b,2));


    TRACE("\nStarting eigenvalue-program.\n");
    int n = 250;
    double s = 1/((double)n+1);

    gsl_matrix *H = gsl_matrix_calloc(n,n);
    for (int i = 0 ; i < n-1; i++) {
        gsl_matrix_set(H,i,i,-2);
        gsl_matrix_set(H,i,i+1,1);
        gsl_matrix_set(H,i+1,i,1);
    }
    gsl_matrix_set(H,n-1,n-1,-2);
    gsl_matrix_scale(H,-1/s/s);




    TRACE("Allocated and filled out H'-matrix.\n");

    gsl_eigen_symmv_workspace *w = gsl_eigen_symmv_alloc(n);

    gsl_vector* eval  = gsl_vector_alloc(n);
    gsl_matrix* evec = gsl_matrix_alloc(n,n);


    TRACE("Allocated Symmv_workspace, vector for eigenvalues and matrix for eigenvectors\n");


    gsl_eigen_symmv(H,eval,evec,w);
    TRACE("Computed eigenvalues and vectors.\n");

    gsl_eigen_symmv_sort(eval,evec,GSL_EIGEN_SORT_VAL_ASC);
    TRACE("Sorted by ascending eigenvalue\n");

    FILE* mystream = fopen("data.txt","w");

    for(int i=0; i<n; i++){
        for(int j=0; j<5; j++){
        double eigenvecval = gsl_matrix_get(evec,i,j);
        fprintf(mystream,"%g\t",eigenvecval);
    }
    fprintf(mystream,"\n");
    }
printf("The eigenenergies are:\n");
    printf ("i   exact   calculated\n");
    for (int k=0; k < 5; k++){
        double exact = pi*pi*(k+1)*(k+1);
        double calculated = gsl_vector_get(eval,k);
        printf ("%i   %g   %g\n", k, exact, calculated);
    }


return 0;
}

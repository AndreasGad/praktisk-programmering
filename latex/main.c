#include <stdio.h>
#include <math.h>
#include <gsl/gsl_integration.h>


double fun(double x, void *params){
    (void) params;
    double f = 2/sqrt(M_PI) * exp(-x*x);
    return f;
}
int main(int argc, char** argv){
double params[4];
for(int i=1; i<argc; i++) {params[i] = atof( argv[i] );}
double size = (params[2]-params[1])/params[3];
double x, result, error;
fprintf(stderr,"\n%g %g %g\n",params[1],params[2],params[3]);
gsl_function errF;
errF.function = &fun;
errF.params = NULL;
gsl_integration_workspace *w = gsl_integration_workspace_alloc(1000);
fprintf(stderr,"%g",size);
printf("\nx       value\n");
for(int i=0; i<size; ++i){
    x = params[1] + i*params[3];
    gsl_integration_qags(&errF,params[0],x,0,1e-7,1000,w,&result,&error);
    printf("%g  %g\n",x,result);
}

gsl_integration_workspace_free(w);
return 0;
}

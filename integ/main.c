#include <stdio.h>
#include <math.h>
#include <gsl/gsl_integration.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_odeiv2.h>

double fun1(double x, void* params){
    double fun  = log(x)/sqrt(x);
return fun;
}
/*
int fun(double x, const double y[], double yprime[], void *params){
    yprime[0] = log(x)/sqrt(x);
return GSL_SUCCESS;
}*/

double energy_fun(double x, void* params){
    double alpha = *(double*) params;
    double f = (-alpha*alpha*x*x/2 + alpha/2 + x*x/2)*exp(-alpha*x*x);
return f;
}

double fun_n(double x, void* params){
    double alpha = *(double*)params;
    double f = exp(-alpha*x*x);
    return f;
}

int main(){
//############################# Start of part A ######################################
    {
    double result, error;
    printf("\nPart A)\n");
    gsl_integration_workspace *w = gsl_integration_workspace_alloc(1000);
    gsl_function funA;
    funA.function = &fun1;

    gsl_integration_qags(&funA,0,1,0,1e-7,1000,w,&result,&error);
    printf("The result of the integration in problem A) is: %g\n",result);
    gsl_integration_workspace_free(w);}
/*
    double t=0;
    double u=0;
    gsl_odeiv2_system diff = {fun, NULL, 1, NULL};
    gsl_odeiv2_driver *driver = gsl_odeiv2_driver_alloc_y_new (
        &diff,gsl_odeiv2_step_rk8pd,1e-3,1e-6,1e-6);
    gsl_odeiv2_driver_apply(driver,&t,1.0,&u);
    printf("The result of the integration in problem A) is: %g\n",u);
    gsl_odeiv2_driver_free(driver);*/
//#############################  End of part A #######################################


//############################# Start of part B ######################################
{
    FILE* data = fopen("data.dat","w");
    fprintf(data, "alpha\tE\n");
    printf("\nPart B)\n");
    gsl_integration_workspace *w = gsl_integration_workspace_alloc(1000);
    double result_H, error_H;
    gsl_function funH;
    funH.function = &energy_fun;
    double result_norm, error_norm;
    gsl_function fun_norm;
    fun_norm.function = &fun_n;

    double alpha = 0;
    double alpha_max = 2;
    double alpha_0 = 0;
    int n = 100;
    double delta_alpha = (alpha_max - alpha_0)/(double)n;
    double E_min = 1e6;
    double alpha_min, Energy;


    for(int i=0; i<n; i++){
        alpha += delta_alpha;
        fun_norm.params = &alpha;
        funH.params = &alpha;
        gsl_integration_qagi(&funH,0,1e-7,1000,w,&result_H,&error_H);
        gsl_integration_qagi(&fun_norm,0,1e-7,1000,w,&result_norm,&error_norm);
        Energy = result_H/result_norm;
        if(Energy<E_min){
            E_min = Energy;
            alpha_min = alpha;
        }
        fprintf(data,"%g\t%g\n",alpha,Energy);
    }
    gsl_integration_workspace_free(w);
printf("The minimum energy is at alpha=%g and E(alpha)=%g\n",alpha_min,E_min);}
//#############################  End of part A #######################################




    return 0;
}

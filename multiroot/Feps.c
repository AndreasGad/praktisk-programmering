#include <stdio.h>
#include <math.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_odeiv2.h>

int H_diff(double r, const double y[], double yprime[], void* params){
    double energy = *(double*)params;
    yprime[0] = y[1];
    yprime[1] = -2*(energy+1/r)*y[0];
    return GSL_SUCCESS;
}


double Feps(double E, double r){
    double rmin = 1e-3;
    if(r<rmin) return r-r*r;
    double hstart = 1e-3, epsabs =1e-6 , epsrel =1e-6 ;
    double y[2] = {rmin-rmin*rmin, 1.-2.*rmin};
    int dimension = 2;
    gsl_odeiv2_system system;
    system.function = H_diff;
    system.jacobian = NULL;
    system.dimension=dimension;
    system.params   = (void*)&E;
    gsl_odeiv2_driver *driver = gsl_odeiv2_driver_alloc_y_new(
        &system,gsl_odeiv2_step_rk8pd,hstart,epsabs,epsrel);
    double t = rmin;
    gsl_odeiv2_driver_apply(driver,&t,r,y);

    gsl_odeiv2_driver_free(driver);
return y[0];
}

#include <stdio.h>
#include <math.h>
#include <gsl/gsl_multiroots.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_odeiv2.h>
#include<gsl/gsl_errno.h>

double Feps(double, double);

int master(const gsl_vector*E,void* params,gsl_vector* f){
    double energy = gsl_vector_get(E,0);
    double rmax = *(double*)params;
    double fval = Feps(energy,rmax);
    double k = sqrt(-2*energy);
    double boundary = -rmax*exp(-k*rmax);
    boundary = 0;
    //boundary = 0;
    fval += boundary;
    gsl_vector_set(f,0,fval);
return GSL_SUCCESS;
}



int rosenbrock_dfun(const gsl_vector *v ,void* params, gsl_vector* f){
(void) params;
double x = gsl_vector_get(v,0);
double y = gsl_vector_get(v,1);
double dx = -2*(1-x)-100*2*(y-x*x)*2*x;
double dy = 100*2*(y-x*x);
gsl_vector_set(f,0,dx);
gsl_vector_set(f,1,dy);
return GSL_SUCCESS;
}


int main(){
//############################ Start of part A #########################################
{
gsl_multiroot_function funA;
funA.f = rosenbrock_dfun;
funA.n = 2;
funA.params = NULL;

gsl_vector *x = gsl_vector_alloc(2);
gsl_vector_set_all(x,0.5);

gsl_multiroot_fsolver *s = gsl_multiroot_fsolver_alloc(gsl_multiroot_fsolver_hybrids,funA.n);
gsl_multiroot_fsolver_set(s,&funA,x);

int status, iter=0;
printf("Part A)\n");
do {
    iter++;
    status = gsl_multiroot_fsolver_iterate(s);
    if(status) break;
    status = gsl_multiroot_test_delta(s->dx,s->f,1e-4,1e-4);
    //printf("\ndx = %g and dy = %g\n",gsl_vector_get(s->dx,0),gsl_vector_get(s->dx,1));
    if(status == GSL_SUCCESS) printf("The iteration has converged and the root is (x,y)=(%g,%g)\n",
        gsl_vector_get(s->x,0),gsl_vector_get(s->x,1));

} while(status == GSL_CONTINUE && iter<1000);


gsl_vector_free(x);
gsl_multiroot_fsolver_free(s);
}//############################ End of part A #########################################
//############################ Start of part B #########################################
{
    fprintf(stderr,"Rmax\tE");
    double rmax_range[] = {0.8, 0.9, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    printf("\nPart B)\n");
for(int i = 0; i < sizeof(rmax_range)/sizeof(rmax_range[0]);i++){
    double rmax = rmax_range[i];
    gsl_multiroot_function funB;
    funB.f = master;
    funB.n = 1;
    funB.params = (void*)&rmax;

    gsl_vector *E = gsl_vector_alloc(1);
    gsl_vector_set_all(E,-2);

    gsl_multiroot_fsolver *s = gsl_multiroot_fsolver_alloc(gsl_multiroot_fsolver_hybrids,funB.n);
    gsl_multiroot_fsolver_set(s,&funB,E);

    int status, iter=0;
    do {
        iter++;
        status = gsl_multiroot_fsolver_iterate(s);
        if(status){ break;}
        status = gsl_multiroot_test_residual(s->f,1e-6);
        if(status == GSL_SUCCESS) printf("The iteration (%2i) has converged and the root is E=%1.2g and F(E)= %1.2g\n",
            iter, gsl_vector_get(s->x,0),gsl_vector_get(s->f,0));
    } while(status == GSL_CONTINUE && iter<1000);

    fprintf(stderr,"%2.2g\t%1.5g\n",rmax,gsl_vector_get(s->x,0));


    gsl_vector_free(E);
    gsl_multiroot_fsolver_free(s);
}

}//############################ End of part B #########################################
    return 0;
}

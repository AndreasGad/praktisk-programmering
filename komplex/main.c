#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "komplex.h"

int main(){

    // Test af komplex_new og komplex_equal.
    komplex z1=komplex_new(1.1,2.2);
    komplex z2=komplex_new(1.1,2.1);
    int ans = komplex_equal(z1,z2);
    if (ans)printf("\nThe numbers %lg%+lg and %lg%+lg are equal\n",z1.re,z1.im,z2.re,z2.im);
    else if (ans==0)printf("\nThe numbers %lg%+lg and %lg%+lg are NOT equal\n",z1.re,z1.im,z2.re,z2.im);
    // Test af komplex_print
    komplex_print("z=",z1);



    komplex a = {1,2}, b = {3,4};

	printf("\ntesting komplex_add...\n");
	komplex r = komplex_add(a,b);
	komplex R = {4,6};
	komplex_print("a=",a);
	komplex_print("b=",b);
	komplex_print("a+b should   = ", R);
	komplex_print("a+b actually = ", r);



    return 0;
}

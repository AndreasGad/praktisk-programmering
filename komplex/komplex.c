#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "komplex.h"
#include "string.h"


#ifndef TAU
#define TAU 1e-6
#endif
#ifndef EPS
#define EPS 1e-6
#endif

const komplex komplex_I = { 0, 1 };


void    komplex_set      (komplex* z, double x, double y){
    z->re=x;        // z er en pointer, og her tilgås re værdien i det objekt z peger på.
    z->im=y;
}  // z ← x+iy

komplex komplex_new      (double x , double y ){
    komplex z = {.re = x, .im = y};
    return z;
}  // returns x+iy

komplex komplex_add      (komplex a, komplex b){
    komplex sum = {.re = a.re + b.re, .im = a.im + b.im};
    return sum;
}  // returns a+b

komplex komplex_sub      (komplex a, komplex b){
    komplex sub = {.re = a.re - b.re, .im = a.im-b.im};
    return sub;
}  // returns a-b

komplex komplex_conjugate   (komplex z){
    komplex conju = {.re = z.re, .im = -z.im};
    return conju;
}  // returns complex conjugate


int double_equal (double a, double b)
{
    if (fabs (a - b) < TAU)
        return 1;
    if (fabs (a - b) / (fabs (a) + fabs (b)) < EPS / 2)
        return 1;
    return 0;
}


int komplex_equal (komplex a, komplex b){
      return double_equal (a.re, b.re) && double_equal (a.im, b.im);
}  // returns 1 if a and b are equal and 0 if they are not.

void komplex_print (char* s, komplex z){
    printf("\n%s %lg%+lgi\n",s,z.re,z.im);
}

komplex komplex_mul (komplex a, komplex b){
    komplex z = {.re = a.re*b.re - a.im*b.im, .im = a.re*b.im + b.re*a.im};
    return z;
}

komplex komplex_div (komplex a, komplex b){
    komplex z = {.re = a.re, .im = b.im};
    return z;
}

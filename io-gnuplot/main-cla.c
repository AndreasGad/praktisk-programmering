#include <stdio.h>
#include <math.h>
#include <stdlib.h>

int main(int argc, char** argv){
    double x;
    for(int i=1;i<argc;i++) {
    	x=atof(argv[i]);
        printf("%f\t%f\n",x,sin(x));
    }
return 0;
}

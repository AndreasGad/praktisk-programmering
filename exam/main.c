#include <stdio.h>
#include <math.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_odeiv2.h>


int tan_diff (double x, const double y[], double yprime[], void *params){
    (void) (x);
    (void) (params);
    yprime[0]=1+y[0]*y[0];
    return GSL_SUCCESS;
}

int main(){
    double t=0.0;
    int dimension = 1;
    double x_min = 0, x_max = M_PI/2., delta_x = (x_max-x_min)/50;
    double hstart = 1e-5, epsabs =1e-8 , epsrel =1e-8 ;
    gsl_odeiv2_system sys = {tan_diff, NULL, dimension, NULL};
    gsl_odeiv2_driver *driver = gsl_odeiv2_driver_alloc_y_new (
        &sys,gsl_odeiv2_step_rk8pd,hstart,epsabs,epsrel);
    double y[1] = {0.0};

    for(double x = x_min; x<x_max; x+=delta_x){
        int status = gsl_odeiv2_driver_apply(driver,&t,x,y);
        double mathtan = tan(x);
        printf ("%lg %lg %lg\n", x, y[0],mathtan);
        if (status != GSL_SUCCESS) fprintf (stderr, "fun: status=%i\n", status);
    }
    gsl_odeiv2_driver_free(driver);
    return 0;
}

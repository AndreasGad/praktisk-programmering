#include <stdio.h>
#include <math.h>
#include <complex.h>


int main(){
double complex z1 = csqrt(-2);
double complex z2 = cexp(I);
double complex z3 = cexp(M_PI*I);
double complex z4 = cpow(I,M_E);


printf("\nPart 1)\n");
printf("Gamma(5)  = %2.5f\n",tgamma(5.0));
printf("J1(0.5)   = %2.5f\n",j1(0.5));
printf("Sqrt(-2)  = %1.2f%+1.2fi\n",creal(z1),cimag(z1));
printf("exp(i)    = %1.2f%+1.2fi\n",creal(z2),cimag(z2));
printf("exp(pi*i) = %1.2f%+1.2fi\n",creal(z3),cimag(z3));
printf("i^e       = %1.2f%+1.2fi\n",creal(z4),cimag(z4));

long double n_ld=0.11111111111111111111111111111111111111111111111111111111111111111L;
double n_d=0.11111111111111111111111111111111111111111111111111111111111111111;
float n_f=0.11111111111111111111111111111111111111111111111111111111111111111;
printf("\nPart 2)\n");
printf("float       = %.25g\n",n_f);
printf("double      = %.25lg\n",n_d);
printf("long double = %.25Lg\n",n_ld);
return 0;
}

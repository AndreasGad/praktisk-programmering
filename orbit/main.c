#include <stdio.h>
#include <math.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_odeiv2.h>

#define TRACE(args...) fprintf(stderr,args)

int diff_function (double x, const double y[], double yprime[], void *params){
    (void) x; // This avoids "unused parameter" warning.
    (void) params;
    yprime[0]=y[0]*(1-y[0]);
    return GSL_SUCCESS;
}

int orbit_func (double phi, const double y[], double yprime[], void *params){
    (void) phi;
    double epsilon = *(double *) params;
    yprime[0] = y[1];
    yprime[1] = 1 - y[0] + epsilon * y[0] * y[0];
    return GSL_SUCCESS;
}

int main(){
    {
     // Problem 1
    double t=0.0;
    int dimension = 1;
    double x_min = 0, x_max = 3, delta_x = (x_max-x_min)/20;
    double hstart = 1e-3, epsabs =1e-6 , epsrel =1e-6 ;
    gsl_odeiv2_system diff = {diff_function, NULL, dimension, NULL};
    gsl_odeiv2_driver *driver = gsl_odeiv2_driver_alloc_y_new (
        &diff,gsl_odeiv2_step_rk8pd,hstart,epsabs,epsrel);
    double y[1] = {0.5};

    for(double x = x_min; x<x_max; x+=delta_x){
        int status = gsl_odeiv2_driver_apply(driver,&t,x,y);
        printf ("%g %g %g\n", x, y[0],1/(1+exp(-x)));
		if (status != GSL_SUCCESS) fprintf (stderr, "fun: status=%i", status);
    }
    gsl_odeiv2_driver_free(driver);}
/*##############################    Part 2     ##################################################*/
    {
    printf("\n\n");
    double t=0.0;
    double hstart = 1e-3, epsabs =1e-6 , epsrel =1e-6 ;
    double params[3];
    double x;
    for(int i = 0; i<3; i++){
        scanf("%lg",&x);
        params[i]=x;}

    int dimension = 2;
    double epsilon = params[0], u[2] = {params[1],params[2]};
    double phi_min = 0, phi_max = 39.5/2 * M_PI, delta_phi = 0.05;
    gsl_odeiv2_system orbit = {orbit_func, NULL, dimension, &epsilon};
    gsl_odeiv2_driver *driver_orbit = gsl_odeiv2_driver_alloc_y_new(
        &orbit,gsl_odeiv2_step_rk8pd,hstart,epsabs,epsrel);

    for(double phi = phi_min; phi<phi_max;phi+=delta_phi){
        int status = gsl_odeiv2_driver_apply(driver_orbit,&t,phi,u);
        printf("%g %g\n",phi,u[0]);
		if (status != GSL_SUCCESS) fprintf (stderr, "fun: status=%i", status);
    }
    gsl_odeiv2_driver_free(driver_orbit);}
    return 0;
}

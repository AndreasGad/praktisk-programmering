#include<stdio.h>
#include<limits.h>
#include<float.h>
int double_equal (double a, double b, double TAU, double EPS);
int main() {
// Opgave 1.1

int i_while=0;
while (i_while+1>i_while){i_while++;}

int i_for=0;
for (i_for; i_for+1 > i_for ; i_for++){}

int i_do_while=0;
do {i_do_while++;} while(i_do_while+1 > i_do_while);

// Opgave 1.2
int i_while_min=0;
while (i_while_min-1<i_while_min){i_while_min--;}

int i_for_min=0;
for (i_for_min; i_for_min-1 < i_for_min ; i_for_min--){}

int i_do_while_min=0;
do {i_do_while_min--;} while(i_do_while_min-1 < i_do_while_min);

// Opgave 1.3
float x_f=1;
while (x_f+1 != 1) {x_f/=2;}
x_f*=2;

double x_d=1;
while (x_d+1 != 1) {x_d/=2;}
x_d*=2;

long double x_ld=1;
while (x_ld+1 != 1) {x_ld/=2;}
x_ld*=2;

printf("INT_MAX: %i\n",INT_MAX);
printf("While: %i\n",i_while);
printf("For: %i\n",i_for);
printf("Do While: %i\n",i_do_while);
printf("\nINT_MIN: %i\n",INT_MIN);
printf("While_min: %i\n",i_while_min);
printf("For_min: %i\n",i_for_min);
printf("Do While_min: %i\n",i_do_while_min);
printf("\nEps_FLT = %lg\n",x_f);
printf("Eps_DBL = %lg\n",x_d);
printf("Eps_LDBL = %Lg\n",x_ld);


// PROBLEM 2
// Float
int max = INT_MAX/3;

float sum_up_FLT   =0;
float sum_down_FLT =0;

for (int i=1; i<max; i++) {
    sum_up_FLT += 1.0f/i;
}

for (int i=max; i>0; i--) {
    sum_down_FLT += 1.0f/i;
}
printf("\nFor Floats\n");
printf("Sum_up = %lg\n",sum_up_FLT);
printf("Sum_down = %lg\n",sum_down_FLT);

// Double
double sum_up_DBL   =0;
double sum_down_DBL =0;

for (int i=1; i<=max; i++) {
    sum_up_DBL += 1.0/i;
}

for (int i=max; i>=1; i--) {
    sum_down_DBL += 1.0/i;
}

printf("\nFor Doubles\n");
printf("Sum_up = %lg\n",sum_up_DBL);
printf("Sum_down = %lg\n",sum_down_DBL);


// Opgave 3

double a = 1.1;
double b = 1.2;
double TAU = 1e-6;
double EPS = 1e-6;
int is_equal = double_equal(a,b,TAU,EPS);
printf("\nThe numbers %lg and %lg are equal/not equal (1/0): %i using the double_equal function.\n",a,b,is_equal);

  return 0;
}

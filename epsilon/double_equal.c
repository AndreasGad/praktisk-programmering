#include <stdio.h>
#include <limits.h>
#include <float.h>
#include <math.h>


int double_equal (double a, double b, double TAU, double EPS)
{
    if (fabs (a - b) < TAU)
        return 1;
    if (fabs (a - b) / (fabs (a) + fabs (b)) < EPS / 2)
        return 1;
    return 0;
}

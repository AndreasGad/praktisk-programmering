#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "nvector.h"


nvector* nvector_alloc (int n){
    nvector* vec = malloc(sizeof(nvector));
    vec -> size  = n;
    vec -> data  = malloc(n*sizeof(double));
    if (vec==NULL) printf("\n There is an error with the vector \n");
    return vec;
}
void    nvector_free(nvector* v)                        {free(v->data); free(v);}
void    nvector_set (nvector* v, int i, double value)   {v->data[i] = value;}
double  nvector_get (nvector* v, int i)                 {return v->data[i];}
double  nvector_dot_product (nvector* v, nvector* u){
    assert(v->size == u->size);
    double product = 0.;
    for (int i=0; i<v->size; i++) product+= (v->data[i]*u->data[i]);
    return product;
}
void nvector_print (char* s, nvector* v) {
    printf("\n%s [",s);
    for(int i=0; i<v->size-1; i++) printf("%lg, ",v->data[i]);
    printf("%lg].\n",v->data[v->size-1]);
}

int double_equal (double a, double b) {if(a == b){return 1;} return 0;}
int nvector_equal (nvector* v, nvector* u) {
    assert(v->size == u->size);
    int res=1;
    for (int i=0; i<v->size; i++) res *= double_equal(v->data[i],u->data[i]);
    return res;
}
void nvector_set_zero (nvector* v){
    for (int i=0; i<v->size;i++) v->data[i]=0;
}
void nvector_add (nvector* v, nvector* u){
    assert(v->size == u->size);
    for (int i=0; i<v->size; i++) v->data[i]+=u->data[i];
}
void nvector_sub (nvector* v, nvector* u){
    assert(v->size == u->size);
    for (int i=0; i<v->size; i++) v->data[i]-=u->data[i];
}
void nvector_scale (nvector* v, double x){
    for (int i=0; i<v->size; i++) v->data[i]*=x;
}
